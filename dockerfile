FROM node:10.15.3

# ENV dir=885KUAS_INST-886KUAS2
WORKDIR /devenv
RUN npm install -g gulp
RUN git clone https://github.com/ExLibrisGroup/primo-explore-devenv
WORKDIR /devenv/primo-explore-devenv
COPY config.js gulp/
RUN npm install
COPY packages/886KUAS_INST-886KUAS2  primo-explore/custom/886KUAS_INST-886KUAS2
COPY packages/VIEW_CODE primo-explore/custom/VIEW_CODE

# ENTRYPOINT [ "gulp", "run", "--view", "886KUAS_INST-886KUAS2", "--ve"]
# ENTRYPOINT [ "gulp", "run", "--view", "886KUAS_INST-886KUAS2"]
ENTRYPOINT [ "gulp", "run", "--view", "VIEW_CODE"]
CMD ["-c"]